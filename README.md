~~pip install --upgrade pip~~
~~python -m venv venv~~
~~./venv/Scripts/activate             (on mac) source venv/bin/activate~~
~~pip install -r requirements.txt~~

## Getting started

Create a virtual environment for the project:  
`python3 -m venv virtualenv`

Then activate the virtual environment:  
`source virtualenv/bin/activate`  
Or on Powershell:  
`.\virtualenv\Scripts\Activate.ps1`

Now install the dependencies for the project:  (doesnt exist yet)
`pip install -r requirements.txt`

You should now be able to run the API with:  
~~`python -m flask run`~~
`python app.py`

In a browser navigate to `http://0.0.0.0:8081/budibase/0000-0002-6968-4610` to see the rendered template
use any other ORCID IDs

## Docker setup

Build and run the docker file with:
`docker compose up --build`


