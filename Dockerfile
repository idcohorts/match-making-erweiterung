# syntax=docker/dockerfile:1
FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV FLASK_ENV="docker"
ENV FLASK_APP="app.py"
ENV FLASK_HOST="0.0.0.0"

COPY . /code/
COPY config-example.yaml /code/config.yaml
WORKDIR /code/

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD ["flask", "run", "--host", "0.0.0.0"]