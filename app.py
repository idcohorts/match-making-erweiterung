from flask import Flask, render_template, request, Response
import json
import io
import yaml
from budibase_client import BudibaseClient
from PIL import Image


#initialize app
app = Flask(__name__)

with open("config.yaml", 'r') as stream:
    config = yaml.safe_load(stream)
host=config["profile_host"]
port=config["profile_port"]
is_debug=config["profile_is_debug"]

service_config = {
    "budibase_url" : f"{config['budibase_host']}",
    "budibase_api_key" : f"{config['budibase_api_key']}"
}
budibase = BudibaseClient(service_config=service_config)
budibase.select_app(config["budibase_app_name"])

print("budibase app selected")
print(host)

#lookup tables
lookups={}
lists = [item for item in config["order"] if "db_name" in item]

for list in lists:
    full_lookups = budibase.get_rows_from_table(list["db_name"])
    lookups[list["db_name"]] = {}
    lookups[list["db_name"]]["rows"] = [{key: dct[key] for key in ['_id', 'name']} for dct in full_lookups]
    lookups[list["db_name"]]["link"] = list["name"]


#landing page
@app.route('/', methods=['GET'])
def index():
    global config
    print(config)

    return render_template('index.html', config=config, request=request)

#to reupload the profile picture
@app.route('/upload_profile_picture', methods=['POST'])
def upload():
    try:
        user_id = request.form['user_id']
        attachment = request.files['avatar.png']

        # Image check
        try:
            image = Image.open(attachment)
        except IOError:
            return Response('File is not an image.', status=400)
        
        # Resolution and size checks
        image = Image.open(attachment)
        if image.width < 200 or image.height < 200:
            return Response('Profile picture must have a minimum size of 200x200.', status=400)
        
        max_size = 8 * 1024 * 1024  # 8 megabytes
        max_size_in_db = 2 * 1024 * 1024 # 2 megabytes
        attachment_size = len(attachment.read())
        attachment.seek(0)
        if attachment_size > max_size:
            return Response('Profile picture must have a maximum size of 8 MB.', status=400)

        # Resize Image to take less space in db
        if attachment_size > max_size_in_db:
            factor = (max_size_in_db / attachment_size) ** 0.5
            width = int(factor * image.width)
            height = int(factor * image.height)

            newImage = image.resize((width, height))

            # Save the resized image to a BytesIO object
            output = io.BytesIO()
            newImage.save(output, format='PNG')
            output.seek(0)

            # Replace the original attachment with the resized image
            attachment = output

        payload = [('file', attachment)]
        # Upload Attachment
        response_image = budibase.upload_attachment(payload, "users")
        # Update user picture with the uploaded image
        row = {"_id": user_id, "picture": response_image}
        budibase.upload_data_to_budibase(row, "users")

        return Response('File uploaded successfully!')
    except Exception as e:
        return Response(f'An error occurred: {str(e)}', status=500)


@app.route('/delete_profile_picture', methods=['POST'])
def delete():
    user_id = request.form['user_id']
    row = {"_id": user_id, "picture": []}
    budibase.upload_data_to_budibase(row, "users")
    return Response('Profile picture deleted successfully!')

#AJAX functions
@app.route('/highlight_publication', methods=['POST'])
def highlight_publication():
    publication_id = request.form['publication_id']
    new_highlighted_value = request.form['new_highlighted_value']
    row = {"_id": publication_id, "is_highlighted": new_highlighted_value}  
    budibase.upload_data_to_budibase(row, "publications")
    return json.dumps({'message': 'Changed publication highlight successfully'})

@app.route('/hide_publication', methods=['POST'])
def hide_publication():
    publication_id = request.form['publication_id']
    new_hidden_value = request.form['new_hidden_value']
    row = {"_id": publication_id, "is_hidden": new_hidden_value}  
    budibase.upload_data_to_budibase(row, "publications")
    return json.dumps({'message': 'Changed publication visibility successfully'})


@app.route('/change-mesh-term-visibility', methods=['POST'])
def change_mesh_term_visibility():
    mesh_term_user_id = request.form['mesh_term_user_id']
    new_value = request.form['new_value']

    row = {"_id": mesh_term_user_id, "is_hidden": new_value}  
    budibase.upload_data_to_budibase(row, "mesh_term_user")

    return json.dumps({'message': 'visibility changed'})


@app.route('/update_user_info', methods=['POST'])
def updateuserinbudibase():

    profile = request.data.decode('utf-8')
    
    # Parse JSON string into a dictionary
    newuserinfo = json.loads(profile)
    # Extract user_id and array
    user_id = newuserinfo['user_id']
    array = newuserinfo['array']
    row = {
        "_id" : user_id,
        "projects": [],
        "organisations": [],
        "fosa": []
    }

    for field in config['order']:
        if field["readonly"] == True:
            continue
        array_fields = [value["name"] for value in array]
        if field["name"] not in array_fields:
            continue

        values = [value["value"] for value in array if value['name'] == field['name']]

        if field["type"] == "text":
            row[field["name"]] = values[0] #?

        elif field["type"] == "list":
            row[field["name"]] = values

        else:
            Exception()


    budibase.upload_data_to_budibase(row, "users")
    return json.dumps({'message': 'information has been uploaded'})



def handle_type_error(e):
    return render_template("usernotfound.html")

@app.route('/profile/<uuid>/edit', methods=['GET', 'POST'])
def render_profile(uuid: str):
    try:
        global lookups
        global config

        users = budibase.get_rows_from_table("users")
        #get only the user  i want
        user = next((d for d in users if d['uuid'] == f'{uuid}'), None)
        print(user["_id"])
        #queries dont help yet
        query = {"query": {"equal": {"user": user["_id"]}}}

        user_data = budibase.retrieve_row_from_table("users", user["_id"])
        if("picture" in user):
            user_data["picture"] = user["picture"]    
        user_data['publications'] = sorted(user_data['publications'], key=lambda k: k['Auto ID'])
    
        user_lookup = lookups.copy()
        for lookup in user_lookup.items():
            user_value = user_data[lookup[1]["link"]]
            for row in lookup[1]["rows"]:
                row["is_selected"] = any([True for item in user_value if item["_id"] == row["_id"] ])

        return render_template( 'tabs.html',           #html template for the page
                                title= f"RPS profile",   #title of the page
                                user = user_data,
                                config = config,    #configuration file for RPS profile
                                lookups=user_lookup, #all organisations
                                request=request,
                                readonly=False,
                                databaseURL=config['budibase_host']
                               )
    except TypeError as e :
        return handle_type_error(e)
    

@app.route('/profile/<uuid>/', methods=['GET', 'POST'])
def render_profile_readonly(uuid: str):
    try:

        global lookups
        global config

        users = budibase.get_rows_from_table("users")
        #get only the user i want
        user = next((d for d in users if d['uuid'] == f'{uuid}'), None)
        print(user["_id"])

        user_data = budibase.retrieve_row_from_table("users", user["_id"])
        if("picture" in user):
            user_data["picture"] = user["picture"] 
        user_data['publications'] = sorted(user_data['publications'], key=lambda k: k['Auto ID'])
        # Send only not hidden publications
        user_data['publications'] = [pub for pub in user_data['publications'] if not pub.get('is_hidden', False)]
    
        user_lookup = lookups.copy()

        for lookup in user_lookup.items():
            user_value = user_data[lookup[1]["link"]]
            for row in lookup[1]["rows"]:
                row["is_selected"] = any([True for item in user_value if item["_id"] == row["_id"] ])

        return render_template( 'tabs.html',           #html template for the page
                                title= f"RPS profile",   #title of the page
                                user = user_data,
                                config = config,    #configuration file for RPS profile
                                lookups=user_lookup, #all organisations
                                request=request,
                                readonly=True,
                                databaseURL=config['budibase_host']
                               )
    except TypeError as e :
        print(e)
        return handle_type_error(e)                      

if __name__ == '__main__':
    app.run_server(host=host, port=port, debug=is_debug)
